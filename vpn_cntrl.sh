#!/usr/bin/env bash

# ADD Variables
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
vpnnssh="$DIR/vpnns.sh" #PATH to vpnns.sh
vpnns="vpnns" #VPN NameSpace Name

function start_vpn() {
    sudo $vpnnssh up
    sudo $vpnnssh start_vpn
}

function stop_vpn() {
    sudo ip netns pids $vpnns | xargs -rd'\n' sudo kill
    sudo $vpnnssh stop_vpn
    sudo $vpnnssh down
}

case "$1" in
    start)
        start_vpn ;;
    stop)
        stop_vpn ;;
    *)
        echo "Syntax: $0 start|stop"
        exit 1
        ;;
esac

