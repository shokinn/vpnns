#!/bin/bash
# Proxy through SSH
# If you want to tunnel through a company proxy use e.g. corkscrew
# Paste this to your SSH config (~/.ssh/config)
#Host <ip> [<ip> <ip> ...] 
#  ProxyCommand /usr/bin/corkscrew <company proxy ip> <company proxy port> [%h %p $HOME/.ssh/.login_credentials]

#Credentials file:
#<user>:<password>



user="" #SSH user name
privatekey="" #Path SSH private key
proxy="" #Proxy server IP or DNS
port="" #SSH server port
localport="" #SOCKS proxy port for appllications

if [ -z "$(screen -ls | grep SSH_TUNNEL)" ]; then
	screen -dmS SSH_TUNNEL ssh -N -D 127.0.0.1:$localport $user@$proxy -p $port -i $privatekey
fi
