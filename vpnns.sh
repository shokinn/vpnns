#!/usr/bin/env bash

if [[ $UID != 0 ]]; then
    echo "This script must be run as root."
    exit 1
fi

if [[ -z "$(which fping)" ]]; then
    echo "You have to install fping"
    exit 1
fi

## Define Variables
vpnns="vpnns" #VPN NameSpace Name.
sevpnclient="/opt/softether/vpnclient" #Path and binary.
sevpndev="vpn_se" #Name of the SoftEther VPN Interface.
extdev="eno1" #en+ match (eno1, eni2, ..), Also common (wl+ for wlan), eth+ (for Ethernet), br+ (for bridges) or you can specify the direct device name like eno1.
nameserver=() #Space seperated list.
vpnextip="" #External address of the VPN server.
vpndefgw="" #Internal (VPN) default gw address.
companyproxy="" #Internal company proxy. If needed.
intfpingsite="" #Internal Website for fping. Only required if you use a company proxy.

function iface_up() {
    ip netns add $vpnns

    ip netns exec $vpnns ip addr add 127.0.0.1/8 dev lo
    ip netns exec $vpnns ip link set lo up

    ip link add vpn0 type veth peer name vpn1
    ip link set vpn0 up
    ip link set vpn1 netns $vpnns up

    ip addr add 172.16.0.1/24 dev vpn0
    ip netns exec $vpnns ip addr add 172.16.0.2/24 dev vpn1
    ip netns exec $vpnns ip route add default via 172.16.0.1 dev vpn1

    iptables -A INPUT \! -i vpn0 -s 172.16.0.0/24 -j DROP
    iptables -t nat -A POSTROUTING -s 172.16.0.0/24 -o $extdev -j MASQUERADE

    sysctl -q net.ipv4.ip_forward=1

    mkdir -p /etc/netns/$vpnns
    touch /etc/netns/$vpnns/resolv.conf
    for i in "${nameserver[@]}"
    do
        echo "nameserver $i" >> /etc/netns/$vpnns/resolv.conf
    done

    if [[ -z "$companyproxy" ]]; then
        ip netns exec $vpnns fping -q google.com
    elif [[ -n "$intfpingsite" ]]; then
        ip netns exec $vpnns fping -q $intfpingsite
    fi
}

function iface_down() {
    rm -rf /etc/netns/$vpnns

    sysctl -q net.ipv4.ip_forward=0

    iptables -D INPUT \! -i vpn0 -s 172.16.0.0/24 -j DROP
    iptables -t nat -D POSTROUTING -s 172.16.0.0/24 -o $extdev -j MASQUERADE

    ip netns delete $vpnns

    ip link delete vpn0
}

function run() {
    shift
    exec sudo ip netns exec $vpnns "$@"
}

function start_vpn() {
    ip netns exec $vpnns ip route add $vpnextip via 172.16.0.1 dev vpn1 proto static
    if [[ -n "$companyproxy" ]]; then
        ip netns exec $vpnns ip route add $companyproxy via 172.16.0.1 dev vpn1 proto static
    fi

    ip netns exec $vpnns $sevpnclient start &

    while ! ip netns exec $vpnns ip a show dev $sevpndev up; do
        sleep .5
    done

    ip netns exec $vpnns dhclient $sevpndev

    ip netns exec $vpnns ip route del default
    ip netns exec $vpnns ip route add default via $vpndefgw dev $sevpndev

    ip netns exec $vpnns fping -q google.com
}

function stop_vpn() {
    ip netns exec $vpnns $sevpnclient stop

    ip netns exec $vpnns ip route del $vpnextip via 172.16.0.1 dev vpn1 proto static
    if [[ -n "$companyproxy" ]]; then
        ip netns exec $vpnns ip route del $companyproxy via 172.16.0.1 dev vpn1 proto static
    fi
    ip netns exec $vpnns ip route del default
    ip netns exec $vpnns ip route add default via 172.16.0.1 dev vpn1
}

case "$1" in
    up)
        iface_up ;;
    down)
        iface_down ;;
    run)
        run "$@" ;;
    start_vpn)
        start_vpn ;;
    stop_vpn)
        stop_vpn ;;
    *)
        echo "Syntax: $0 up|down|run|start_vpn|stop_vpn"
        exit 1
        ;;
esac
