#!/usr/bin/env bash

# Check if Variable $1 filled
if [[ -z $1 ]]; then
	echo "You have to choose a program."
	exit 1
fi

# ADD Variables
vpnns="vpnns" #VPN NameSpace Name

# set proxy to nothing
unset http_proxy
unset https_proxy
unset no_proxy
unset HTTP_PROXY
unset HTTPS_PROXY
unset NO_PROXY

# Popcorn time!
sudo ip netns exec $vpnns sudo -u $USER $1 
